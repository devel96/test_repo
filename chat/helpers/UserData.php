<?php

namespace common\servers\chat\helpers;

use common\models\User;

class UserData
{

    /**
     * select receiver auth key for using it in chat
     *
     * @param array $data
     * @return array
     */
    public static function prepareReceiverData(array $data)
    {
        if (isset($data['receiver']['id'])) {
            $data['receiver']['id'] = User::find()
                ->where(['id' => $data['receiver']['id']])
                ->select('auth_key')->one()->auth_key;
        }
        return $data;
    }
}