<?php


namespace common\servers\chat\models;



use common\models\mongo\messages\ConversationFactory;
use common\models\mongo\messages\ConversationRepository;
use common\models\mongo\messages\Message;
use yii\helpers\ArrayHelper;

class ChatRepository
{
    /**
     * @var string
     */
    private $senderId;

    /**
     * @var string
     */
    private $receiverId;

    /**
     * @var string
     */
    private $message;

    /**
     * @var array
     */
    private $users = [];

    /**
     * @return string
     */
    public function getSenderId(): string
    {
        return $this->senderId;
    }

    /**
     * @param string $senderId
     */
    public function setSenderId(string $senderId): void
    {
        $this->senderId = $senderId;
    }

    /**
     * @return string
     */
    public function getReceiverId(): string
    {
        return $this->receiverId;
    }

    /**
     * @param string $receiverId
     */
    public function setReceiverId(string $receiverId): void
    {
        $this->receiverId = $receiverId;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage(string $message): void
    {
        $this->message = $message;
    }

    public function __construct(string $senderId, string $receiverId, ?string $message = '')
    {
        $this->senderId = $senderId;
        $this->receiverId = $receiverId;
        $this->message = $message;
        $this->users = [$this->senderId, $this->receiverId];
    }

    /**
     *
     */
    public function setMessagesRead(): void
    {
        if($this->senderId !== $this->getLastMessageSenderId()) {
            foreach ($this->getAllUnreadConversations() as $message) {
                /** @var Message $message */
                $message->is_read = 1;
                $message->read_at = time();
                $message->save();
            }
        }
    }

    private function getAllUnreadConversations(): array
    {
        print_r($this->users);

        print_r(Message::find()->select(['message', 'sender_id', 'receiver_id'])
            ->where(['sender_id' => $this->users, 'receiver_id' => $this->users, 'is_read' => 0])
            ->addOrderBy(['created_at' => SORT_DESC])->all());
        return Message::find()->select(['message', 'sender_id'])
            ->where(['sender_id' => $this->users, 'receiver_id' => $this->users, 'is_read' => 0])
            ->addOrderBy(['created_at' => SORT_DESC])->all();
    }

    /**
     * @return int
     */
    public function getLastMessageReadTime(): int
    {
        return Message::find()->select(['read_at'])
            ->where(['sender_id' => $this->users, 'receiver_id' => $this->users])
            ->asArray()->addOrderBy(['created_at' => SORT_DESC])->one()['read_at'] ?? 0;
    }

    /**
     * @return string
     */
    public function getLastMessageSenderId(): string
    {
        return Message::find()->select(['message', 'sender_id'])
            ->where(['sender_id' => $this->users, 'receiver_id' => $this->users])
            ->asArray()->addOrderBy(['created_at' => SORT_DESC])->one()['sender_id'] ?? '';
    }

    /**
     * @return bool
     */
    public function addNewMessage(): bool
    {
        $message = new Message();
        $message->sender_id = $this->senderId;
        $message->receiver_id = $this->receiverId;
        $message->message = $this->message;
        $message->is_read = ChatServerCommand::MESSAGE_UNREAD;
        $message->created_at = time();

        if($message->save(false)) {
            $receiverConversationObject = ConversationFactory::create($message->sender_id);
            $repository = new ConversationRepository($receiverConversationObject);
            $receiverSaved = $repository->addConversation($message->receiver_id, $message->created_at);

            $senderConversationObject = ConversationFactory::create($message->receiver_id);
            $repository = new ConversationRepository($senderConversationObject);
            $senderSaved = $repository->addConversation($message->sender_id, $message->created_at);

            return $receiverSaved && $senderSaved;
        }

        return false;
    }


    /**
     * @return array
     */
    public function getLastMessages(): array
    {
        return Message::find()->select(['message', 'sender_id'])
            ->where(['sender_id' => $this->users, 'receiver_id' => $this->users])
            ->limit(5)->asArray()->addOrderBy(['created_at' => SORT_DESC])->all();
    }


}