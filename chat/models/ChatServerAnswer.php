<?php


namespace common\servers\chat\models;

/**
 * Class ChatServerAnswer
 * @package common\servers\chat\models
 *
 */
class ChatServerAnswer
{
    /**
     * @var array
     */
    private $data;

    /**
     * ChatServerAnswer constructor.
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->setData($data);

        return $this;
    }

    /**
     * @param array $data
     * @return $this
     */
    public function setData(array $data): self
    {
        $this->data = $data;

        return $this;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * json encode the data
     *
     * @return string
     */
    public function toJson(): string
    {
        return json_encode($this->getData());
    }
}