<?php


namespace common\servers\chat\models;


use common\servers\chat\ChatServer;
use Ratchet\ConnectionInterface;
use Ratchet\WebSocket\Version\RFC6455\Connection;

class CommandEvent
{
    /**
     * @var ChatServer
     */
    private $chatServer;
    /**
     * @var ConnectionInterface
     */
    private $senderConnection;
    /**
     * @var int
     */
    private $sender;
    /**
     * @var int
     */
    private $receiver;
    /**
     * @var string
     */
    private $senderMessage;
    /**
     * @var int|string
     */
    private $senderConnectionName;

    /**
     * CommandEvent constructor.
     * @param ChatServer $chatServer
     */
    public function __construct(ChatServer $chatServer, ConnectionInterface $senderConnection, array $data)
    {
        $this->setChatServer($chatServer);

        $this->setSenderConnection($senderConnection);

        $this->setData($data);
    }

    /**
     * @param ConnectionInterface $senderConnection
     */
    private function setSenderConnection(ConnectionInterface $senderConnection): void
    {
        $this->senderConnection = $senderConnection;
        $this->senderConnectionName = $senderConnection->resourceId;
    }

    /**
     * @param array $data
     */
    private function setData(array $data): void
    {
        $this->sender = $data['sender'] ?? null;
        $this->receiver = $data['receiver'] ?? null;
        $this->senderMessage = $data['message'] ?? null;
    }

    /**
     * @param ChatServer $chatServer
     * @return $this
     */
    public function setChatServer(ChatServer $chatServer): self
    {
        $this->chatServer = $chatServer;

        return $this;
    }

    /**
     * @return ChatServer
     */
    public function getChatServer(): ChatServer
    {
        return $this->chatServer;
    }

    /**
     * =====================================================================================
     * =====================================================================================
     * ===============================          Commands        ============================
     * =====================================================================================
     * =====================================================================================
     */

    /**
     * Register new Client in socks array
     */
    public function registerNewClient(): void
    {
        try {
            /**
             * if the sender exists
             */
            if ($this->sender) {

                $senderId = (string)$this->sender['id'];

                /**
                 * Check if the user connected first time
                 * add in connectedUsers private property
                 */
                if (!$this->chatServer->getClientByConnectionName($this->senderConnectionName)) {
                    $this->chatServer->setConnectedUsers($this->senderConnectionName, $senderId);
                    $this->chatServer->setChatClients($this->senderConnectionName, $this->senderConnection);
                    echo "New connection! ({$this->senderConnectionName})\n";
                }
            }
        } catch (\Exception $e) {
            print_r($e->getMessage());
        }

    }

    /**
     * This method called every time when user send a message
     */
    public function registerNewMessage(): void
    {
        try {
            $receiverId = (string)$this->receiver['id'];
            $senderId = (string)$this->sender['id'];

            /**
             * @var Connection $receiver
             */
            $receiver = $this->chatServer->getConnectionByUserId($receiverId);

            if ($receiver && $this->senderConnection) {

                $chatRepo = new ChatRepository($senderId, $receiverId, $this->senderMessage);

                if ($chatRepo->addNewMessage()) {
                    $sendData = new ChatServerAnswer([
                        'command' => ChatServerCommand::MESSAGE,
                        'messages' => $chatRepo->getLastMessages(),
                        'sender' => $this->sender,
                        'receiverId' => $receiverId
                    ]);


                    /**
                     * send the data
                     */
                    $receiver->send($sendData->toJson());
                    $this->senderConnection->send($sendData->toJson());
                }
            } else {
                $chatRepo = new ChatRepository($senderId, $receiverId, $this->senderMessage);

                if ($chatRepo->addNewMessage()) {
                    $sendData = new ChatServerAnswer([
                        'command' => ChatServerCommand::MESSAGE,
                        'messages' => $chatRepo->getLastMessages(),
                        'sender' => $this->sender,
                        'receiverId' => $receiverId
                    ]);

                    /**
                     * send the data
                     */
                    $this->senderConnection->send($sendData->toJson());
                }

            }
        } catch (\Exception $e) {
            print_r($e->getMessage());
        }

    }

    /**
     * This method called every time when user types in chat
     */
    public function registerIsWriting(): void
    {
        try {
            $receiverId = (string)$this->receiver['id'];
            $senderId = (string)$this->sender['id'];

            /**
             * @var Connection $receiver
             */
            $receiver = $this->chatServer->getConnectionByUserId($receiverId);

            if ($receiver && $this->senderConnection) {
                $sendData = new ChatServerAnswer([
                    'command' => ChatServerCommand::ISWRITING,
                    'messages' => [
                        [
                            'message' => $this->sender['username'] . " пишет..."
                        ]
                    ],
                    'sender' => $this->sender,
                    'receiverId' => $receiverId
                ]);

                /**
                 * send the data
                 */
                $receiver->send($sendData->toJson());
            }
        } catch (\Exception $e) {
            print_r($e->getMessage());
        }

    }

    /**
     * This method called every time when user read the message
     */
    public function registerNewRead(): void
    {
        try {
            $receiverId = (string)$this->receiver['id'];
            $senderId = (string)$this->sender['id'];


            $chatRepo = new ChatRepository($senderId, $receiverId);
            $chatRepo->setMessagesRead();

            /**
             * @var Connection $receiver
             */
            $receiver = $this->chatServer->getConnectionByUserId($receiverId);

            $chatRepo = new ChatRepository($senderId, $receiverId);
            $chatRepo->setMessagesRead();

            if ($receiver && $this->senderConnection) {
                $sendData = new ChatServerAnswer([
                    'command' => ChatServerCommand::READ,
                    'messages' => [
                        [
                            'message' => "Прочитано в " . date("d.m.Y H:i", $chatRepo->getLastMessageReadTime())
                        ]
                    ],
                    'lastMessageSenderId' => $chatRepo->getLastMessageSenderId(),
                    'sender' => $this->sender,
                    'receiverId' => $receiverId
                ]);

                /**
                 * send the data
                 */
                $receiver->send($sendData->toJson());
            }
        } catch (\Exception $e) {
            print_r($e->getMessage());
        }
    }

}